var createGrid = function(size){
	var grid = $("#grid");
	var innerStr = "";
	for(var i = 0; i < size; i++)
	{
		innerStr += "<div class='gridCol'>";
		for(var j = 0; j < size; j++)
		{
			innerStr += "<div class='gridSquare'></div>"
		}
		innerStr += "</div>";
	}

	grid.html(innerStr);

	var squareSize = (960/size)-2;
	var squares = $(".gridSquare");
	squares.css("height", squareSize);
	squares.css("width", squareSize);
	
	squares.mouseenter(function(){
		$(this).addClass("hovered");
	});
	/*$(".gridSquare").mouseleave(function(){
		$(this).removeClass("hovered");
	});*/
}

$("document").ready(function(){
	
	createGrid(16);

	$("#createGrid").click(function(){
		var response = prompt("Enter number of squares (1-96)", "16");
		if(response != null || repsonse != "")
		{
			var digit = parseInt(response) || 0;
			if(digit > 0)
			{
				$("#grid").html("");
				createGrid(digit);
			}
		}
	});

});